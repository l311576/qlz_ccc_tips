// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    target: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    private moveSpeed = 200;

    start () {

    }

    setTarget(target){
        this.target = target;
        this.update(0);
    }

    update (dt) {
        if(!this.target){
            return;
        }
        //计算出朝向
        var dx = this.target.x - this.node.x;
        var dy = this.target.y - this.node.y;
        var dir = cc.v2(dx,dy);
        if(dir.magSqr() < 100*100){
            this.node.destroy();
            return;
        }

        var angle = dir.signAngle(cc.v2(1,0));

        var degree = angle / Math.PI * 180;
        this.node.rotation = degree;

        dir.normalizeSelf();
        this.moveSpeed += dt * 30;
        this.node.x += dt * dir.x * this.moveSpeed;
        this.node.y += dt * dir.y * this.moveSpeed;
    }
}
