// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html


var AnimConfig = { }
AnimConfig['0001'] = {
    'attack':{frames:8,fps:8},
    'attacked':{frames:1,fps:8},
    'combat_idle':{frames:4,fps:8},
    'idle':{frames:4,fps:8},
    'ride_idle':{frames:4,fps:8},
    'ride_run':{frames:8,fps:8},
    'run':{frames:8,fps:8},
    'rush':{frames:1,fps:8},
    'spell1':{frames:8,fps:8},
    'spell2':{frames:8,fps:8},
    'spell3':{frames:14,fps:8},
    'spell4':{frames:8,fps:8},
    'spell5':{frames:4,fps:8},
    'spell6':{frames:10,fps:8},
}

AnimConfig['0003'] = {
    'attack':{frames:8,fps:8},
    'attacked':{frames:1,fps:8},
    'combat_idle':{frames:4,fps:8},
    'idle':{frames:4,fps:8},
    'ride_idle':{frames:4,fps:8},
    'ride_run':{frames:8,fps:8},
    'run':{frames:8,fps:8},
    'rush':{frames:1,fps:8},
    'spell1':{frames:8,fps:8},
    'spell2':{frames:8,fps:8},
    'spell3':{frames:14,fps:8},
    'spell4':{frames:8,fps:8},
    'spell5':{frames:4,fps:8},
    'spell6':{frames:10,fps:8},
}

export default class NewClass {
    public static getRoleInfo(roleId){
        return AnimConfig[roleId];
    }

}
