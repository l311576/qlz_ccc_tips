//背景图缩放器。用于缩放背景
cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        //适配模式
        //
        //
        //
        scaleMethod:0,
    },

    onLoad(){
        this.resize();
        cc.find('Canvas').on('resize',function(){
            this.resize();
        }.bind(this));
    },

    // LIFE-CYCLE CALLBACKS:
    resize () {
        //0、居中（居中其实不需要挂这个脚本，浪费效率）
        //1、宽高都根据高度拉伸
        //2、长边充满
        var cvs = cc.find('Canvas').getComponent(cc.Canvas);
        var size = cc.view.getFrameSize();
        //
        var dr = cvs.designResolution;
        var scaleMethod = this.scaleMethod;

        //
        var fitWidth = true;
        //如果更宽，则使用定高
        if((size.width/size.height) > (dr.width / dr.height)){
            fitWidth = false;
        }

        //自由缩放撑满
        if(scaleMethod == 1){
            if(fitWidth){
                this.node.height = this.node.width/size.width * size.height;
            }
            else{
                this.node.width = this.node.height/size.height * size.width;
            }
        }
        //保持等比缩放撑满
        else if(scaleMethod == 2){
            if(fitWidth){
                var oldHeight = this.node.height;
                this.node.height = this.node.width/size.width * size.height;
                this.node.width = this.node.height/oldHeight * this.node.width;
            }
            else{
                var oldWidth = this.node.width;
                this.node.width = this.node.height/size.height * size.width;
                this.node.height = this.node.width / oldWidth * this.node.height;
            }
        }
        else{
            //默认处理，有黑边
        }
    },

    // update (dt) {},
});
